package ru.kolesnikov.tm.constant;

public final class TerminalConst {

    public static final String CMD_HELP = "help";

    public static final String ARG_HELP = "-h";

    public static final String CMD_VERSION = "version";

    public static final String ARG_VERSION = "-v";

    public static final String CMD_ABOUT = "about";

    public static final String ARG_ABOUT = "-a";

    public static final String CMD_EXIT = "exit";

    public static final String CMD_INFO = "info";

    public static final String ARG_INFO = "-i";

}
